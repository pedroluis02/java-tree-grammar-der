/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package der;

import java.util.ArrayList;

/**
 *
 * @author Pedro Luis
 */

public class ContenedorDerNivel {
    ArrayList <DerNivel> nivelDer;
    
    public ContenedorDerNivel(){
        nivelDer = new ArrayList <DerNivel>();
    }
    public ArrayList <DerNivel> getConNivel() {
        return nivelDer;
    } 
    public DerNivel getConDN(int pos){
        return nivelDer.get(pos);
    }
    public void addDerN(DerNivel ln){
        nivelDer.add(ln);
    }
    public void resetear(){
        nivelDer.clear();
    }
    public int size(){
        return nivelDer.size();
    }
        
    //-- Der Nivel ------------------------------
    public static class DerNivel{
        private ArrayList <Integer> dernivel;
        public DerNivel(){
            dernivel = new ArrayList <Integer>();
        }
         public DerNivel(ArrayList <Integer> der){
             this.dernivel = der;
         }        

        public ArrayList <Integer> getDerNivel(){
            return dernivel;
        } 
        public void addDn(int pos){
            dernivel.add(pos);
        }
    }
}
