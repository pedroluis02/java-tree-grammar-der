/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gram;

import java.util.ArrayList;

/**
 *
 * @author Pedro Luis
 */
public class DersElementos {
    
    //lista de der por cada regla : posiciones de ElmentosG
    private int posId;
    private ArrayList <Integer> derRs;
    
    public DersElementos(int posId){
        this.posId = posId;
        this.derRs = new ArrayList <Integer>();
    }
    
    //-------------------------------------
    public void addEder(int pos){
        derRs.add(pos);
    }
    public void addLeder(ArrayList <Integer> edeRs){
        this.derRs = edeRs;
    }
    //-------------------------------------
    public void setposId(int posId){
        this.posId = posId;
    }
    //-------------------------------------
    public int getEder(int pos){
        return derRs.get(pos);
    }
    public ArrayList <Integer> getDerRs(){
        return derRs;
    }
    public int getposId(){
        return posId;
    }
}
