/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gram;

import java.util.ArrayList;

/**
 *
 * @author Pedro Luis
 */

public class ElementoG {
    private String nombre;
    private int pos;
    private boolean terminal;
    private ArrayList <Integer> der;
    
    public ElementoG(String nombre, int pos, boolean terminal){
        this.nombre = nombre;
        this.pos = pos;
        this.terminal = terminal;
        this.der = new ArrayList <Integer>();
    }
    public ElementoG(String nombre, int pos){
        this(nombre, pos, false);
    }
    public void setTerminal(boolean terminal){
        this.terminal = terminal;
    }
    public boolean isTerminal(){
        return terminal;
    }
    //---------------------------------------
    public void addDer(int idDer){
        der.add(idDer);
    }
    //---------------------------------------
    public String getNombre(){
        return nombre;
    }
    public int posRS(){
        return pos;
    }
    public ArrayList <Integer> getDers(){
        return der;
    }
}
