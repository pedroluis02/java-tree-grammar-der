/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gram;

import der.ContenedorDerNivel;
import gramlist.ListaDersElementos;
import gramlist.ListaElementosG;
import java.util.ArrayList;
import java.util.Iterator;
import uti.Conf;

/**
 * @author Pedro Luis
 */

public class Gramatica {
    
    // der de la gramatica : De las reglas
    public ListaDersElementos lde;
    // terminales y no terminales : lista
    public ListaElementosG leg;
    // ders nivel
    private ContenedorDerNivel nls;
    private ContenedorDerNivel auxNL;
    private String cadena_A;
    private int tipo;
    private int menor;
    private boolean TER;
    
    public Gramatica(){ 
        lde = new ListaDersElementos();
        leg = new ListaElementosG();
        nls = new ContenedorDerNivel();
        auxNL = new ContenedorDerNivel();
        cadena_A = "";
        tipo = Conf.SHOW_TREE;
        menor = 0;
        TER = true;
    }
    
    private Conf obtenerDerStr(Iterator <Integer> lder, 
            ArrayList <Integer> datoA){ 
        Conf conf = new Conf();
        while(lder.hasNext()){
            ElementoG lga = leg.getElg(lder.next());

            if(lga.isTerminal()) {
                conf.cadena += lga.getNombre();
            }
            else{
                conf.noterminal = true;
                conf.cadena += "<"+lga.getNombre()+">";
            }

            datoA.add(lga.posRS());
        }
        return conf;
    }
    private int datoStrNivel(ArrayList <Integer> listader, int posD,
            ElementoG eg){
        String aux = ""; ElementoG egaux;
        Iterator <Integer> dersEI = eg.getDers().iterator();
        int i; boolean noterminal = false;
               
        while(dersEI.hasNext()){
            i = 0;
            ArrayList <Integer> datoN = new ArrayList <Integer> ();
            while(i < listader.size()){
                
                int pos = listader.get(i);
                //System.out.print(pos);
                if(posD == i){
                    int posI = dersEI.next();
                    Iterator <Integer> der_i = 
                            lde.getDer(posI).getDerRs().iterator();
                    Conf conf = obtenerDerStr(der_i, datoN);
                    aux += conf.cadena;
                    if(conf.noterminal && !noterminal){
                        noterminal = true;
                    }
                }
                else{
                    egaux = leg.getElg(pos);
                    
                    if(!egaux.isTerminal()){
                        noterminal = true;
                        aux += "<"+egaux.getNombre()+">";
                    }
                    else{
                        aux += egaux.getNombre();
                    }
                    datoN.add(egaux.posRS());
                }
                i++;
            }
            if(noterminal){
                auxNL.addDerN(new ContenedorDerNivel.DerNivel(datoN));
            }

            if(tipo == Conf.SHOW_TREE){
                System.out.print("\t"+aux);
            }
            else if(tipo == Conf.EXISTS_STR){
                if(!noterminal){
                    TER = false;
                    System.out.println("\t"+aux);
                    if(aux.compareTo(cadena_A)==0){
                        return Conf.EXISTS;
                    }
                    if(aux.length() < cadena_A.length()){
                        menor++;
                    }
                }
            }
            aux = "";
            noterminal = false;
        }
        return Conf.NOT_STR;
    }
    private ElementoG elementoIncial(){
       int posInicial = leg.getPosInicial();

        ElementoG einicial = leg.getElg(posInicial);

        ContenedorDerNivel.DerNivel dni = 
                new ContenedorDerNivel.DerNivel();

        dni.addDn(posInicial);
        nls.addDerN(dni); // nivel 0
        return einicial;
}
    private void actualizarDatosNivel(){
        nls.resetear();
        for(int k=0; k<auxNL.getConNivel().size(); k++){
        nls.addDerN( new ContenedorDerNivel.DerNivel(
                (ArrayList <Integer>) 
                    auxNL.getConDN(k).getDerNivel().clone()
                ));
        }
        auxNL.resetear();
    }
    private int ejecutar_der(){
        Iterator <ContenedorDerNivel.DerNivel> listaD = 
                   nls.getConNivel().iterator();
        
        while(listaD.hasNext()){ // Lista Nivel Anterior
               ContenedorDerNivel.DerNivel dernivel = 
                       listaD.next();
               ArrayList <Integer> derI = dernivel.getDerNivel();

               int j = 0;
               while(j < derI.size()){// Lista de Datos de Nivel
                   ElementoG eg = leg.getElg(derI.get(j));
                                   
                   if(!eg.isTerminal()){
                       int op = datoStrNivel(derI, j, eg);
                       
                       if(tipo == Conf.EXISTS_STR){
                           if(op == Conf.EXISTS || op == Conf.NOT_EXISTS){
                              return op; 
                           }
                       }
                   }
                   j++;
               }
           }
           if(tipo == Conf.SHOW_TREE){
               System.out.println();
           }
//           System.out.println("MENOR: "+menor);
           actualizarDatosNivel();
           
           if(TER){
               return Conf.NOT_STR;
           }
           else if(menor > 0){
               menor = 0;
               return Conf.NOT_STR;
           }
           else{
               return Conf.NOT_EXISTS;
           }
    }
    public void derNivel(int nivel){

       ElementoG einicial = elementoIncial();
       System.out.println("\t"+einicial.getNombre());
       
       tipo = Conf.SHOW_TREE;
       
       int i = 0;
       while(i<nivel){ // --- IT -----------------------------------------------
           ejecutar_der();
           i++;
       }// -- END IT -----------------------------------------------------------
    }
    public void existe_cadena(String cadena){
        cadena_A = cadena;
        tipo =   Conf.EXISTS_STR;
        
        elementoIncial();
        
        int op = Conf.NOT_EXISTS;
        while(true){
           op = ejecutar_der(); 
           if(op == Conf.EXISTS || op == Conf.NOT_EXISTS){
               break;
           }
        }
        if(op == Conf.EXISTS){
            System.out.println("Existe");
        }
        else if(op == Conf.NOT_EXISTS){
            System.out.println("No Existe");
        }
    }
    //--------------------------------------------------------------------------
    public void iniciarDers(){
        nls.resetear();
        auxNL.resetear();
        TER = true;
    }
    //--------------------------------------------------------------------------
    public void imprimirLista(ArrayList l){
        Iterator it = l.iterator();
        while(it.hasNext()){
            System.out.println(it.next());
        }
    }
}
