/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gramlist;

import gram.DersElementos;
import java.util.ArrayList;

/**
 *
 * @author Pedro Luis
 */

public class ListaDersElementos {
    
    //lista de Ders por cada regla
    ArrayList <DersElementos> ders;
    
    public ListaDersElementos(){
        ders = new ArrayList <DersElementos>();
    }
    //----------------------------------------
    public void addDer(int posId, ArrayList <Integer> derRs){
        DersElementos aux = new DersElementos(posId);
        aux.addLeder(derRs);
        addDer(aux);
    }
    public void addDer(DersElementos derEl){
        ders.add(derEl);
    }
    //----------------------------------------
    public DersElementos getDer(int posId){
        return ders.get(posId);
    }
}
