/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gramlist;

import gram.ElementoG;
import java.util.ArrayList;

/**
 *
 * @author Pedro Luis
 */

public class ListaElementosG {
    // terminales y no terminales
    private int posInicial;
    private ArrayList <ElementoG> elementos;
    
    public ListaElementosG(){
        elementos = new ArrayList <ElementoG> ();
    }
    
    //-----------------------------------------
    public void addElg(ElementoG eg){
        elementos.add(eg);
    }
    //-----------------------------------------
    public void setPosInicial(int posInicial){
        this.posInicial = posInicial;
    }
    //-----------------------------------------
    public int getPosInicial(){
        return posInicial;
    }
    public ElementoG getElg(int pos){
        return elementos.get(pos);
    }
    public ArrayList <ElementoG> getListaEG(){
        return elementos;
    }
}
