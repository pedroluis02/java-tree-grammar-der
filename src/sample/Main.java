/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package sample;

import gram.ElementoG;
import gram.Gramatica;
import java.io.DataInputStream;
import java.io.IOException;
import java.util.ArrayList;
import uti.Conf;

/**
 *
 * @author pedroluis
 */

public class Main {

    /**
     * @param args the command line arguments
     */
    private static void leerDatosAR(Gramatica g, DataInputStream dis){
        
        int i = 0;
        System.out.println("Simbolos Terminales o alfabeto");
        System.out.println("\t Tecle ';' para salir.\n");
        String datoA = null;
        while(true){
            try {
                System.out.print("datoA["+i+"]: ");
                datoA = dis.readLine();
            } catch (IOException ex) {
                System.out.println("Error de ingreso: "+ex.getMessage());
                continue;
            }
            if(datoA.compareTo(";")==0)
                break;
            g.leg.addElg(new ElementoG(datoA, i, true));
            i++; 
        }
        
        System.out.println("Simbolos No Terminales o Reglas");
        System.out.println("\t Tecle ';' para salir.\n");
        String datoR = null;

        while(true){
            try {
                System.out.print("datoR["+i+"]: ");
                datoR = dis.readLine();
            } catch (IOException ex) {
                System.out.println("Error de ingreso: "+ex.getMessage());
                continue;
            }
            if(datoR.compareTo(";")==0)
                break;
            g.leg.addElg(new ElementoG(datoR, i));
            i++; 
        }
        leerDers(g, dis);
    }
    private static void leerDers(Gramatica g, DataInputStream dis){
        int i = 0, j = 0;
        ArrayList <ElementoG> els = g.leg.getListaEG();
        ElementoG aux; 
        System.out.println("Reglas ingreso de DERS");
        System.out.println("\t Tecle ';' para salir.\n");
        while(i < els.size()){
            aux = els.get(i);
            if(aux.isTerminal()){
                i++;
                continue;
            }
            System.out.print(aux.getNombre()+" -> \n");
            String der = "";
            while(true){
                try {
                    System.out.print("\t : ");
                    der = dis.readLine();
                } catch (IOException ex) {
                    System.out.println("Error de ingreso: "+ex.getMessage());
                    continue;
                }
               if(der.compareTo(";")==0){
                   break;
               }
               g.lde.addDer(j, Conf.convertirLista(der.split(" "), els));
               g.leg.getElg(i).addDer(j);
               j++;
            }
            System.out.println();
            i++;
        }
        
        System.out.println();
        
        String posInicial = "";
        try{
            System.out.print("\t Elemento Inicial: ");
            posInicial = dis.readLine();
        }
        catch(IOException ex){
            System.out.println("Error de ingreso: "+ex.getMessage());
        }
        int posi = Conf.buscarElementoG(els, posInicial);
        g.leg.setPosInicial(posi);
        
        System.out.println();
    }
    
    public static void main(String[] args) {
        // TODO code application logic here
        DataInputStream dis = new DataInputStream(System.in);
        
        Gramatica gram = new Gramatica();
//        gram.leg.addElg(new ElementoG("a", 0));
//        gram.leg.addElg(new ElementoG("b", 1));
//        gram.leg.addElg(new ElementoG("c", 2));
//        
//        gram.leg.addElg(new ElementoG("E", 3));
//        gram.leg.addElg(new ElementoG("W", 4));
//        
//        //----------------------------------------------------------------------
//        ArrayList <Integer> derI1 = new ArrayList <Integer>();
//        derI1.add(4); derI1.add(0); derI1.add(4); gram.lde.addDer(0, derI1);
//        
//        ArrayList <Integer> derI2 = new ArrayList <Integer>();
//        derI2.add(1); derI2.add(1); derI2.add(4); gram.lde.addDer(1, derI2);
//        
//        ArrayList <Integer> derI3 = new ArrayList <Integer>();
//        derI3.add(2); gram.lde.addDer(2, derI3);
//        //----------------------------------------------------------------------
//        gram.leg.getElg(0).setTerminal(true);
//        gram.leg.getElg(1).setTerminal(true);
//        gram.leg.getElg(2).setTerminal(true);
//         
//        gram.leg.getElg(3).addDer(0);
//         
//        gram.leg.getElg(4).addDer(1);
//        gram.leg.getElg(4).addDer(2);
//         
//        gram.leg.setPosInicial(3);
        
        leerDatosAR(gram, dis);
        
        char op = '.';
        int nivel = 0;
        String cadena = "";
        while(op!='S'){
            System.out.print("\t GRAMATICA \n" +
                    "\n [D].- Ver Niveles"+
                    "\n [B].- Cadena"+
                    "\n [S].- Salir \n"+
                    "\t :: ");
            try{
                
                op = dis.readLine().charAt(0);
            }catch(IOException ex){
                System.out.println("Error de ingreso: "+ex.getMessage());
            }
            switch(op){
                case 'd':
                case 'D':
                    try{
                        System.out.print("\t nivel: ");
                        nivel = Integer.parseInt(dis.readLine());
                    }
                    catch(IOException ex){
                        System.out.println("Error de ingreso: "+ex.getMessage());
                    }
                    System.out.println();
                    gram.derNivel(nivel);
                    break;
                case 'b':
                case 'B': 
                    try{
                        System.out.print("\t Cadena: ");
                        cadena = dis.readLine();
                    }catch(IOException ex){
                        System.out.println("Error de ingreso: "+ex.getMessage());
                    }
                    System.out.println();
                    gram.existe_cadena(cadena);
                    break;
                case 's':
                case 'S': 
                    System.exit(0);
                    break;
            }
            System.out.println("\n");
            gram.iniciarDers(); 
        }
        
        //gram.derNivel(5);    
        //gram.existe_cadena("cacc");
    }
}
