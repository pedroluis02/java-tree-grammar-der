/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uti;

import gram.ElementoG;
import java.util.ArrayList;

/**
 *
 * @author Pedro Luis
 */
public class Conf {
    
    public String cadena;
    public boolean noterminal;
        
    public Conf(){
        cadena = "";
        noterminal = false;
    }
    
    //tipo
    public static int SHOW_TREE  = 0; // ver arbol
    public static int EXISTS_STR = 1; // realizar derivacion
    
    //cadena
    public static int EXISTS = 2; // existe
    public static int NOT_EXISTS = 3; // no existe
    public static int ERROR = 4; // error de cadena
    public static int NOT_STR = 5; // aun no se halla coincidencia
    
    public static boolean compararListas(ArrayList <Integer> l1,
            ArrayList <Integer> l2){
        int ls1 = l1.size();
        int ls2 = l2.size();
        if(ls1!=ls2)
            return false;
        int i = 0;
        while(i<ls1){
            if(l1.get(i)!=l2.get(i))
                return false;
        }
        return true;
    }
    public static int buscarElementoG(ArrayList <ElementoG> els, String nombre){
        int i = 0;
        ElementoG aux;
        while(i < els.size()){
            aux = els.get(i);
            if(nombre.compareTo(aux.getNombre())==0)
               return aux.posRS();
            i++;
        }
        return -1;
    }
    public static ArrayList <Integer> convertirLista(String datos[], 
            ArrayList <ElementoG> le){
        ArrayList <Integer> lisint = new ArrayList<Integer>();
        int i = 0;
        while(i<datos.length){
            int posId = buscarElementoG(le, datos[i]);
            //System.out.println(": "+posId+" - "+datos[i]);
            lisint.add(posId);
            i++;
        }
        return lisint;
        
    }
}
